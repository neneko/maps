
import argparse
import os
import json

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--infiles', '-i', required=True, nargs='*', action='append')
parser.add_argument('--results_dir', default='./data/',  )
opt = parser.parse_args()

num_files = len(opt.infiles[0])
assert num_files > 1, 'Seems given only one input file.' + \
  " If not so, confirm you correctly type the command: `python thisfile.py -i file1 file2', not `python thisfile.py -i file1 -i file2'."

list_jsons = []
outfile_prefix = []
for idx, infile in enumerate(opt.infiles[0]):

    assert infile.endswith('.json'), "The file name must end with '.json'." + \
                            " '{}' cannot be loaded.".format(infile)

    print infile
    with open(infile, 'r') as f:
        list_jsons += json.load(f)

    outfile_prefix += [os.path.basename(infile).strip('.json')]


outfile = opt.results_dir + '_'.join(outfile_prefix) + '.json'
with open(outfile, 'w') as f:
    json.dump(list_jsons, f, indent=2)

print 'Successfully saved as %s' % outfile 
