
commit_message="$@"
#echo $commit_message
commit_message_escaped=${commit_message/\//\\/}
#echo $commit_message_escaped
commit_message_escaped=${commit_message_escaped/'/\'\'/}
#echo $commit_message_escaped

echo $commit_message > last_commit_message.log

sed -i -e "s/neneko_placeholder/$commit_message_escaped/" ./map.js
git add .
sed -i -e "s/$commit_message_escaped/neneko_placeholder/" ./map.js

git commit -m "$commit_message"
git push -u origin master

