# -*- coding: utf-8 -*-
import json
import geojson
import sys
import os

filepath = sys.argv[1]
if os.path.exists(filepath):
    filename = os.path.basename(filepath)
    if filename.endswith(".json"):
        filename = filename.strip(".json")
    else:
        exit("Input file needs to be .json file.")
else:
    exit("File not found")
#with open("./gyouza_full.json") as f:
with open(filepath) as f:
    data = json.load(f)

[u'name',
u'latitude',
u'region',
u'longitude',
u'genre',
u'score',
u'link',
u'date',
u'station',
u'holiday',
u'review']

[u'holiday',
u'tel',
u'name',
u'paymethod',
u'region',
u'isReservable',
u'budget',
u'longitude',
u'latitude',
u'station',
u'openhours',
u'date',
u'seats',
u'genre',
u'link',
u'score',
u'review']


arr = []
for datum in data:
    arr.append(geojson.Feature(
        geometry=geojson.Point((float(datum["latitude"]), float(datum["longitude"]))), 
        properties={
            'title':datum['name'],
            'region':datum['region'],
            'station':datum['station'],
            'genre':datum['genre'],
            'score':datum['score'],
            'holiday':datum['holiday'],
            'link':datum['link'],
            'review':datum['review'],
            'date':datum['date'],
            'tel':datum['tel'],
            'paymethod':datum['paymethod'],
            'isReservable':datum['isReservable'],
            'budget':datum['budget'],
            'openhours':datum['openhours'],
            'seats':datum['seats'],
            }
        )
    )

result = geojson.FeatureCollection(arr)
with open("./data/" + filename + ".geojson", "w") as f:
    geojson.dump(result, f, indent=2)
    #geojson.dump(result, f, ensure_ascii=False, indent=2)
