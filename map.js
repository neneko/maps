
var map
var markers = []
alert('Add Tochigi gyouza shops');
//
// start out with filter features set to false, so no filtering happens by default
//var filters = {shower:false, vault:false, flush:false}
var filters = {'3.5':false, '3.3':false, '3.1':false, '3.0':false}

var genre_dict = {}
var region_dict = {}

var scoreFilterIsActive = false
var genreFilterIsActive = false
var regionFilterIsActive = false

var filter_condition = "検索条件："

///////////////////////////////////////////////////////////
///////////////////// Filter by score /////////////////////
///////////////////////////////////////////////////////////
$(function () {
  $('input[name=filter]').change(function (e) {
    //console.log("HOGE\n" + this.id + " " + this.value)
    map_filter(this.id);
    set_filters = get_set_options()
    scoreFilterIsActive = (set_filters.length != 0)
    filter_markers_by_score(set_filters)
  });
})

var map_filter = function(id_val) {
  if (filters[id_val]) 
    filters[id_val] = false
  else
    filters[id_val] = true
}

var get_set_options = function() {
  ret_array = []
  for (option in filters) {
    if (filters[option]) {
      ret_array.push(option)
    }
  }
  return ret_array;
}

var filter_markers_by_score = function() {  
  
  // for each marker, check to see if all required options are set
  for (i = 0; i < markers.length; i++) {
    marker = markers[i];

    // start the filter check assuming the marker will be displayed
    // if any of the required features are missing, set 'keep' to false
    // to discard this marker
    //keep=true
    marker.visibilityByScore = true
    num_matches = 0
    score = marker.properties['score']

    if (set_filters.length == 0) {
      marker.visibilityByScore = false
    }

    //if(i==1){
    //  console.log("score" + score)
    //  console.log("set_filters" + set_filters)
    //  for (opt=0; opt<set_filters.length; opt++) {
    //    console.log(set_filters[opt])
    //  }
    //}

    for (opt=0; opt<set_filters.length; opt++) {
      if (set_filters[opt] == '3.5') {
        if(score < 3.5){
          //keep = false;
          marker.visibilityByScore = false
          num_matches++;
    	}
      } else if (set_filters[opt] == 3.3) {
      if (score < 3.3 || score >= 3.5){
          //keep = false;
          marker.visibilityByScore = false
          num_matches++;
    	}
      } else if (set_filters[opt] == 3.1) {
      if (score < 3.1 || score >= 3.3){
          //keep = false;
          marker.visibilityByScore = false
          num_matches++;
      	}
      } else if (set_filters[opt] == 3.0) {
      if (score < 3.0 || score >= 3.1){
          //keep = false;
          marker.visibilityByScore = false
          num_matches++;
      	}
      }
    }
    if (num_matches != set_filters.length){
      //keep = true
      marker.visibilityByScore = true
    }
    keep = false
    if (!regionFilterIsActive && scoreFilterIsActive && !genreFilterIsActive) {
      keep = marker.visibilityByScore
    } else if (!regionFilterIsActive && scoreFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByScore && marker.visibilityByGenre)
    } else if (regionFilterIsActive && scoreFilterIsActive && !genreFilterIsActive) {
      keep = (marker.visibilityByRegion && marker.visibilityByScore)
    } else if (regionFilterIsActive && scoreFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByRegion && marker.visibilityByScore && marker.visibilityByGenre)
    } else if (!scoreFilterIsActive && regionFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByGenre && marker.visibilityByRegion)
    } else if (!scoreFilterIsActive && !regionFilterIsActive && genreFilterIsActive) {
      keep = marker.visibilityByGenre
    } else if (!scoreFilterIsActive && regionFilterIsActive && !genreFilterIsActive) {
      keep = marker.visibilityByRegion
    }


    //if (set_filters.length != 0){
    //  if (marker.visibilityByScore && marker.visibilityByGenre && marker.visibilityByRegion)
    //      keep = true
    //} else {
    //  if (marker.visibilityByGenre && marker.visibilityByRegion)
    //      keep = true
    //}
    marker.setVisible(keep)
    //marker.setVisible(keep)
    //marker.setVisible(marker.visibility)
  }
}
///////////////////////////////////////////////////////////
///////////////////// Filter by genre /////////////////////
///////////////////////////////////////////////////////////
$(function(){
    var values = []
    $('#multiSelectByGenre').change(function (e) {
        var current = $(this).find('option:selected').map(function () {
            return $(this).val()
        })
        var newValue = $(current).not(values).get();
        values = current;
        
        //console.log("\nnewValue: " + newValue);
        //console.log("curValues: " + $(current).get());

        var currentValues = $(current).get()
        genreFilterIsActive = (currentValues.length != 0)

        filter_markers_by_genre(currentValues)
    }) 
})

var filter_markers_by_genre = function (currentValues) {
  // for each marker, check to see if all required options are set
  for (i = 0; i < markers.length; i++) {
    marker = markers[i];

    // start the filter check assuming the marker will be displayed
    // if any of the required features are missing, set 'keep' to false
    // to discard this marker
    //var keep=true
    marker.visibilityByGenre = true
    var num_matches = 0
    genres = marker.properties['genre'].split("、")

    if (currentValues.length == 0) {
        marker.visibilityByGenre = false
    }
    for (opt=0; opt<currentValues.length; opt++) {
        if (genres.indexOf(currentValues[opt]) == -1) {
            //keep = false
            marker.visibilityByGenre = false
            num_matches++
        }
    }
    if (num_matches != currentValues.length){
      //keep = true
      marker.visibilityByGenre = true
    }
    //marker.setVisible(keep)
    keep = false
    if (!regionFilterIsActive && !scoreFilterIsActive && genreFilterIsActive) {
      keep = marker.visibilityByGenre
    } else if (regionFilterIsActive && !scoreFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByRegion && marker.visibilityByGenre)
    } else if (!regionFilterIsActive && scoreFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByGenre && marker.visibilityByScore)
    } else if (regionFilterIsActive && scoreFilterIsActive && genreFilterIsActive) {
      keep = (marker.visibilityByRegion && marker.visibilityByScore && marker.visibilityByGenre)
    } else if (!genreFilterIsActive && scoreFilterIsActive && regionFilterIsActive) {
      keep = (marker.visibilityByScore && marker.visibilityByRegion)
    } else if (!genreFilterIsActive && !scoreFilterIsActive && regionFilterIsActive) {
      keep = marker.visibilityByRegion
    } else if (!genreFilterIsActive && scoreFilterIsActive && !regionFilterIsActive) {
      keep = marker.visibilityByScore
    }


    //if (currentValues.length != 0){
    //  if (marker.visibilityByScore && marker.visibilityByGenre && marker.visibilityByRegion)
    //  //if (marker.visibilityByRegion)
    //      keep = true
    //} else {
    //  if (marker.visibilityByScore && marker.visibilityByRegion)
    //      keep = true
    //}
    marker.setVisible(keep)
  }
}

////////////////////////////////////////////////////////////
///////////////////// Filter by region /////////////////////
////////////////////////////////////////////////////////////
$(function(){
    var values = []
    $('#multiSelectByRegion').change(function (e) {
        var current = $(this).find('option:selected').map(function () {
            return $(this).val()
        })
        var newValue = $(current).not(values).get();
        values = current;
        
        //console.log("\nnewValue: " + newValue);
        //console.log("curValues: " + $(current).get());

        var currentValues = $(current).get()
        regionFilterIsActive = (currentValues.length != 0)

        filter_markers_by_region(currentValues)

        //filter_condition = "検索条件："
        if (regionFilterIsActive) {
          //$('#jump').css({'pointer-events':'auto', 'opacity':'1'})
          $('#jump').css({'pointer-events':'auto', 'background':'ghostwhite', 'opacity':'1'})
          //filter_condition = filter_condition + "地域が" + currentValues.join(" or ")
          if (scoreFilterIsActive) {
            if (genreFilterIsActive) {
              keep = (marker.visibilityByRegion && marker.visibilityByScore && marker.visibilityByGenre)
            } else if (!genreFilterIsActive) {
              keep = (marker.visibilityByRegion && marker.visibilityByScore)
            }
          } else if (!scoreFilterIsActive) {
            if (genreFilterIsActive) {
              keep = (marker.visibilityByRegion && marker.visibilityByGenre)
            } else if (!genreFilterIsActive) {
              keep = marker.visibilityByRegion
            }
          }
        } else if (!regionFilterIsActive) {
          $('input[id=jump]').css({'pointer-events':'none', 'opacity':'0.5'})
          //$('input[id=jump]').css({'pointer-events':'none', 'opacity':'0.5'})
          if (scoreFilterIsActive) {
            if (genreFilterIsActive) {
              keep = (marker.visibilityByScore && marker.visibilityByGenre)
            } else if (!genreFilterIsActive) {
              keep = marker.visibilityByScore
            }
          } else if (!scoreFilterIsActive) {
            if (genreFilterIsActive) {
              keep = marker.visibilityByGenre
            } else if (!genreFilterIsActive) {
            }
          }
        }
        //$('#filter_condition').html(filter_condition)
        //
        //marker.addListener('click', function() {
          //$('#restaurant_info').html(markerInfo)
        //});
    }) 
})

var filter_markers_by_region = function (currentValues) {
  // for each marker, check to see if all required options are set
  for (i = 0; i < markers.length; i++) {
    marker = markers[i];

    // start the filter check assuming the marker will be displayed
    // if any of the required features are missing, set 'keep' to false
    // to discard this marker
    //var keep=true
    marker.visibilityByRegion = true
    var num_matches = 0
    region = marker.properties['region']

    if (currentValues.length == 0) {
        marker.visibilityByRegion = false
    }
    for (opt=0; opt<currentValues.length; opt++) {
        if (region.indexOf(currentValues[opt]) == -1) {
            //keep = false
            marker.visibilityByRegion = false
            num_matches++
        }
    }
    if (num_matches != currentValues.length){
      //keep = true
      marker.visibilityByRegion = true
    }
    //marker.setVisible(keep)
    keep = false

    if (regionFilterIsActive) {
      if (scoreFilterIsActive) {
        if (genreFilterIsActive) {
          keep = (marker.visibilityByRegion && marker.visibilityByScore && marker.visibilityByGenre)
        } else if (!genreFilterIsActive) {
          keep = (marker.visibilityByRegion && marker.visibilityByScore)
        }
      } else if (!scoreFilterIsActive) {
        if (genreFilterIsActive) {
          keep = (marker.visibilityByRegion && marker.visibilityByGenre)
        } else if (!genreFilterIsActive) {
          keep = marker.visibilityByRegion
        }
      }
    } else if (!regionFilterIsActive) {
      if (scoreFilterIsActive) {
        if (genreFilterIsActive) {
          keep = (marker.visibilityByScore && marker.visibilityByGenre)
        } else if (!genreFilterIsActive) {
          keep = marker.visibilityByScore
        }
      } else if (!scoreFilterIsActive) {
        if (genreFilterIsActive) {
          keep = marker.visibilityByGenre
        } else if (!genreFilterIsActive) {
        }
      }
    }

    marker.setVisible(keep)

  }
}


/////////////////////////////////////////////////////
///////////////////// Utilities /////////////////////
/////////////////////////////////////////////////////
//$(function fixInfoWindow() {
//    // replaced with "clickableIcons: false"
//    var set = google.maps.InfoWindow.prototype.set;
//    google.maps.InfoWindow.prototype.set = function(key, val) {
//        if (key === "map") {
//            if (! this.get("noSuppress")) {
//                return;
//            }
//        }
//        set.apply(this, arguments);
//    }
//});

$(function () {
  $('input[id=jump]').click(function (e) {
    var maxLat = 0
    var minLat = 999
    var maxLng = 0
    var minLng = 999
    //var wasUpdated = false
    for (i = 0; i < markers.length; i++) {
      marker = markers[i];
      if (marker.visibilityByRegion) {
        var lat = marker.position['lat']()
        var lng = marker.position['lng']()
        //wasUpdated = true
        if (lat > maxLat) {
          maxLat = lat
        }
        if (lng > maxLng) {
          maxLng = lng
        }
        if (lat < minLat) {
          minLat = lat
        }
        if (lng < minLng) {
          minLng = lng
        }
      }
    }
    //if (wasUpdated) {
    var southWest = new google.maps.LatLng(minLat, minLng);
    var northEast = new google.maps.LatLng(maxLat, maxLng);
    var bounds = new google.maps.LatLngBounds(southWest,northEast);
    //map.setCenter(bounds.getCenter())
    //map.panTo(bounds.getCenter())
    //map.panToBounds(bounds)
    map.fitBounds(bounds);
    //} else {
    //  alert("地域が選択されていません．")
    //}

  });
});


////////////////////////////////////////////////////////
///////////////////// Load markers /////////////////////
////////////////////////////////////////////////////////
// after the geojson is loaded, iterate through the map data to create markers
// and add the pop up (info) windows
function loadMarkers() {
  //console.log('creating markers')
  //alert('creating markers')
  var infoWindow = new google.maps.InfoWindow()

  //geojson_url = './data/gyouza_full_180328_1.geojson'
  //geojson_url = './data/cafe_kichijoji.geojson'
  //geojson_url = './data/cafe_kichijoji_gyouza_full.geojson'
  //geojson_url = './data/cafe_full.json'
  //geojson_url = './data/cafe_kichijoji.json'
  //geojson_url = './data/gyouza_shinbashi.json'
  //geojson_url = './data/cafe_shinbashi.json'
  //geojson_url = './data/cafe_kichijoji_gyouza_full.json'
  //geojson_url = './data/cafe_shinbashi_gyouza_shinbashi.json'
  //geojson_url = './data/cafe_full_gyouza_full.json'
  geojson_url = './data/cafe_full_gyouza_full_gyouza_tochigi.json'
  $.getJSON(geojson_url, function(result) {
    // Post select to url.
    data = result


    //var loadedPoints = 0
    //var totalPoints = data.length

    //var bar = document.getElementById("progress-bar")
    //var txt = document.getElementById('pct')
    //function progress() {
    //  if (loadedPoints >= totalPoints) {
    //    clearInterval(id)
    //  } else {
    //    loadedPoints++;
    //    //alert(loadedPoints + "/" + totalPoints)
    //    bar.value = Math.ceil((loadedPoints / totalPoints)*100)
    //	txt.value = bar.value + '% loaded (' + loadedPoints + "/" + totalPoints + ')'
    //  }
    //}

    $.each(data, function(key, val) {
      var point = {lat: parseFloat(val['latitude']), lng: parseFloat(val['longitude'])}
      //var point = new google.maps.LatLng(parseFloat(val['latitude']), parseFloat(val['longitude']));
      var titleText        = val['name']
      var regionText       = val['region']
      var stationText      = val['station']
      val['genre'] = val['genre'].replace(/^\s+|\s+$/g, '')
      var genreText        = val['genre']
      var scoreText        = val['score']
      var holidayText      = val['holiday']
      var linkText         = val['link']
      var reviewText       = val['review']
      var dateText         = val['date']
      var telText          = val['tel']
      var paymethodText    = val['paymethod']
      var isReservableText = val['isReservable']
      var budgetText       = val['budget']
      var openhoursText    = val['openhours']
      var seatsText        = val['seats']

      if(scoreText >= 3.5){
          color = 'F25C00'
      }else if(scoreText >= 3.3 && scoreText < 3.5){
          color = 'F9A603'
      }else if(scoreText >= 3.1 && scoreText < 3.3){
          color = 'FFDB5C'
      }else if(scoreText >= 3.0 && scoreText < 3.1){
          color = 'F7F5E6'
      }else{
          color = 'F7EFE2'
      }

      icon = "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|" + color
      var marker = new google.maps.Marker({
        //position: latLngLiteral,
        position: point,
        title: titleText,
        map: map,
        icon: icon,
        //zIndex: 1,
        //size: new google.maps.Size(5, 5),
        //properties: val['properties'],
        properties: val,
        visibilityByScore: true,
        visibilityByGenre: true,
        visibilityByRegion: true,
      });

      marker.setVisible(false)
      //if (val['region'] == '六本木・麻布・広尾' || val['region'] == '浜松町・田町・品川')
      //  if (val['score'] >= 3.5)
      //    marker.setVisible(true)
        //marker.visibilityByRegion = true

      //markers[0].properties['genre'].split("、")[0].replace(/^\s+|\s+$/g, '').match("中華")
      genres = genreText.split("、")
      for (id in genres) {
        g = genres[id].replace(/^\s+|\s+$/g, '')
        genre_dict[g] = false
      }
      region_dict[val['region']] = false

      //var markerInfo = "<div><h3>" + titleText + "</h3>Amenities: " + descriptionText + "</div>"
      //var markerInfo = "<div><h3>" + titleText + "</h3><a href=\""+linkText+" \">" + linkText + "</a></div>"
      var rstName = '<h4>'+titleText+'<br>'
      var rstScore = scoreText+' ('+reviewText+')</h4>'
      var rstGenre = '【分類】' + genreText + '<br>'
      var rstStation = '【最寄駅】' + stationText + '<br>'
      var rstHoliday = '【定休日】' + holidayText + '<br>'
      var rstLink = '<a href=\"'+linkText+'\">'+linkText+'</a><br>'
      var rstTel = '【TEL】' + '<a href=\"tel:'+telText.replace(/-/g, "")+'\">' + telText + '</a><br>'
      var rstPaymethod = '【支払い方法】' + paymethodText + '<br>'
      var rstIsReservable = '【予約可否】' + isReservableText + '<br>'
      var rstBudget = '【予算】' + budgetText + '<br>'
      var rstOpenhours = '【営業時間】' + openhoursText + '<br>'
      var rstSeats = '【席数】' + seatsText + '<br>'

      var markerInfo = '<div>' + rstName + rstScore + rstLink + rstGenre + rstStation + rstHoliday + rstTel + rstPaymethod + rstIsReservable + rstBudget + rstOpenhours + rstSeats + '</div>'

      marker.addListener('click', function() {
        $('#restaurant_info_body').fadeIn()
        $('#restaurant_info_body').html(markerInfo)
        $('#restaurant_info_close').css('display', 'inline')
        $('#restaurant_info_close').click(function () {
          //$('#restaurant_info_body').animate({opacity: 0})
          $('#restaurant_info_body').fadeOut('fast')
          $('#restaurant_info_close').css('display', 'none')
        })
      });
      marker.addListener('click', toggleBounce)
      markers.push(marker)

      //id = setInterval(progress(), 0.01)

      function toggleBounce() {
        marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function(){ marker.setAnimation(null); }, 750)
      }

    });

    (function (){
      $.each(genre_dict, function (i, item){
        $('#multiSelectByGenre').append($('<option>').text(i).val(i)).multiselect('refresh')
      })
      $.each(region_dict, function (i, item){
        $('#multiSelectByRegion').append($('<option>').text(i).val(i)).multiselect('refresh')
      })
    })()

  });
  //alert('Makers have been created.')
}


jQuery(function($) {
    jQuery("#multiSelectByGenre").multiselect({
        selectedList: 100,
        checkAllText: "全選択",
        uncheckAllText: "全選択解除",
        noneSelectedText: "ジャンル（未選択です）",
        selectedText: "# 個選択",
        minWidth: 450,
        height: 1000,
    });
});
jQuery(function($) {
    jQuery("#multiSelectByRegion").multiselect({
        selectedList: 100,
        checkAllText: "全選択",
        uncheckAllText: "全選択解除",
        noneSelectedText: "エリア（未選択です）",
        selectedText: "# 個選択",
        minWidth: 450,
        height: 1000,
    });
});


// グローバル変数
var syncerWatchPosition = {
    count: 0 ,
    lastTime: 0 ,
    map: null ,
    marker: null ,
    current_pos: null,
} ;

function initMap() {
    map_options = {
      zoom: 15,
      center: {lat: 35.658581, lng: 139.745433},
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.LEFT_CENTER,
      },
      fullScreenControl: false,
      fullScreenControlOptions: {
        position: google.maps.ControlPosition.BOTTOM_CENTER,
      },
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER,
      },
      clickableIcons: false,
    }
    
    // original
    map_document = document.getElementById('map')
    map = new google.maps.Map(map_document,map_options);
    loadMarkers()
    // original

    if (!navigator.geolocation) {
      alert('Geolocation APIに対応していません．')

      //map_opttions.center = {lat: 35.658581, lng: 139.745433}
      map_document = document.getElementById('map')
      map = new google.maps.Map(map_document,map_options);
      var center = new google.maps.LatLng(35.658581, 139.745433)
      map.setCenter(center)
      loadMarkers()

    } else {
      // Get the current position
      //navigator.geolocation.getCurrentPosition(
      navigator.geolocation.watchPosition(
        function(position) {
          // データの更新
          ++syncerWatchPosition.count ;                   // 処理回数
          //var nowTime = ~~( new Date() / 1000 ) ; // UNIX Timestamp
          //alert('Now: ' + nowTime)

          // 前回の書き出しから3秒以上経過していたら描写
          // 毎回HTMLに書き出していると、ブラウザがフリーズするため
          //if( (syncerWatchPosition.lastTime + 3) > nowTime )
          //{
          //    alert('WatchPosition skipped')
          //    return false ;
          //}

          // 前回の時間を更新
          //syncerWatchPosition.lastTime = nowTime ;

          var current_position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
          syncerWatchPosition.current_pos = current_position

          //map_document = document.getElementById('map')
          //map = new google.maps.Map(map_document,map_options);

          //var marker = new google.maps.Marker({
          //  position: current_position,
          //  map: map,
          //})

          //map.setCenter(current_position)
          //loadMarkers()

          //if (map == null) {
          if (syncerWatchPosition.marker == null) {
            //map_document = document.getElementById('map')
            //map = new google.maps.Map(map_document,map_options);
            syncerWatchPosition.marker = new google.maps.Marker({position: current_position, map: map})
            map.setCenter(current_position)

            accuracy_circle = new google.maps.Circle({
              map: map,
              center: current_position,
              radius: position.coords.accuracy,
              strokeColor: '#0088ff',
              strokeWeight: 1,
              fillColor: '#0088ff',
              fillOpacity: 0.2,
            })
            //accuracy_circle.setVisible(false)

            //alert('Initial mapping')
            //loadMarkers()
          }
          else {
            //map.setCenter(current_position)
            syncerWatchPosition.marker.setPosition(current_position)
            //accuracy_circle.setMap(null)
            accuracy_circle.setRadius(position.coords.accuracy)
            accuracy_circle.setCenter(current_position)
            accuracy_circle.setMap(map)
            $(function () {
              $('input[id=current_position]').click(function (e) {
                map.setCenter(current_position)
              });
            });
          }

        }, 
        function(error) {
          var error_msg = {
            0: "原因不明のエラーが発生しました．",
            1: "位置情報の利用が許可されていません．", 
            2: "デバイスの位置が判定できません．", 
            3: "タイムアウトしました．",
          }
          alert('位置情報取得に失敗しました．\n' + 'Errno: ' + error.code + '\n' + error_msg[error.code] + '\n' + error.message)

          //map_document = document.getElementById('map')
          //map = new google.maps.Map(map_document,map_options);
          //var center = new google.maps.LatLng(35.658581, 139.745433)
          //map.setCenter(center)
          //loadMarkers()
        },
        {enableHighAccuracy: true, timeout: 300000, maximumAge: 30000}
      );
    }
}

google.maps.event.addDomListener(window, 'load', initMap);

